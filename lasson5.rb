myname = 'Zed A. Shaw'
myage = 35  # not a lie in 2009
myheight = 74  # inches
myweight = 180  # lbs
myeyes = 'Blue'
myteeth = 'White'
myhair = 'Brown'

puts "Let's talk about #{myname}."
puts "He's #{myheight} inches tall."
puts "He's #{myweight} pounds heavy."
puts "Actually that's not too heavy."
puts "He's got #{myeyes} eyes and #{myhair} hair."
puts "His teeth are usually #{myteeth} depending on the coffee."

# this line is tricky, try to get it exactly right
puts "If I add #{myage}, #{myheight}, and #{myweight} I get #{myage + myheight + myweight}"


#convert from inches to centimeters
myheightC = myheight * 2.54
#convert from pound to kilogram
myweightK = myweight / 2.2
#print the new parameters
puts "myheight in centimeters is #{myheightC} and myweight in kilogram is #{myweightK}"
