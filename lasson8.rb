# create a formater
formater = "%{first} %{second} %{third} %{fourth}"
# insert inside formater digit
puts formater % {first: 1,second: 2,third: 3,fourth: 4}
# insert inside formater STRING
puts formater % {
  first: "bla",
  second: "sdf",
  third: "asdf",
  fourth: "sdfgv"
}
